package com.example.warcaby;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.InputType;
import android.widget.EditText;

import androidx.annotation.RequiresApi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Score {

    ArrayList<String> list;
    File scoreTable;

    String player;
    int scoreOne;
    int scoreTwo;



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void getScores(Context context) {
        list = new ArrayList<>();
        scoreTable = new File(context.getApplicationContext().getFilesDir(),"scoreTable.txt");

        try (BufferedReader br = new BufferedReader(new FileReader(scoreTable))) {
            String line;
            while ((line = br.readLine()) != null && !line.isEmpty()) {
                list.add(line);
            }
        } catch (IOException ex) {
            try {
                scoreTable.createNewFile();
            }
            catch (IOException ex1) {
                System.out.println("\nProblem with reading Score File." + ex);
            }
        }

        Collections.sort(list, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return extractInt(o2) - extractInt(o1);
            }

            int extractInt(String s) {
                String num = s.substring(0,3);
                String num2 = num.replaceAll("\\D", "");
                return num2.isEmpty() ? 0 : Integer.parseInt(num2.trim());
            }
        });

        if(list.size() > 10) {
            for (int i = list.size() - 1; i > 9; i--) {
                list.remove(i);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void scoreSave(Context context) {
        getScores(context);
        list.add(scoreOne + " points    " + player);
        list.add(scoreTwo + " points    Player 2");
        String s = "";
        for (int i = 0; i<list.size(); i++) {
            s = s + list.get(i) + "\n";
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(scoreTable))) {
            writer.write(s);
        } catch (IOException ex) {
            System.out.println("\nProblem with saving Score File.");
        }
        Intent i = new Intent(context, Leaderboard.class);
        context.startActivity(i);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)

    public void setName(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Player 1, What's Your name?");

        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                player = input.getText().toString();
                scoreSave(context);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                player = "Player 1";
                scoreSave(context);
            }
        });

        builder.show();
    }

    public void addScoreOne(int score) {
        scoreOne = scoreOne + score;
    }

    public void addScoreTwo(int score) {
        scoreTwo = scoreTwo + score;

    }

    public void resetScores() {
        scoreOne = 0;
        scoreTwo = 0;
    }

}
