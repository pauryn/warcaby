package com.example.warcaby;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Leaderboard extends AppCompatActivity {

    ArrayList<String> list;
    ArrayList<String> leaderboardList;
    File scoreTable;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaderboard);

        final ListView listView = (ListView) findViewById(R.id.listView);
        scoreTable = new File(getApplicationContext().getFilesDir(),"scoreTable.txt");
        list = new ArrayList<>();
        leaderboardList = new ArrayList<>();

        scoreString();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, leaderboardList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                TextView item = (TextView) super.getView(position,convertView,parent);
                item.setTypeface(Typeface.MONOSPACE);
                item.setTextColor(Color.parseColor("#684033"));
                item.setTypeface(item.getTypeface(), Typeface.BOLD);
                item.setTextSize(TypedValue.COMPLEX_UNIT_DIP,18);
                return item;
            }
        };
        listView.setAdapter(adapter);

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void scoreString() {
        list.clear();
        leaderboardList.clear();

        try (BufferedReader br = new BufferedReader(new FileReader(scoreTable))) {
            String line;
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
        } catch (IOException ex) {
            try {
                scoreTable.createNewFile();
            }
            catch (IOException ex1) {
                System.out.println("\nProblem with reading Score File." + ex);
            }
        }

        Collections.sort(list, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return extractInt(o2) - extractInt(o1);
            }

            int extractInt(String s) {
                String num = s.substring(0,3);
                String num2 = num.replaceAll("\\D", "");
                return num2.isEmpty() ? 0 : Integer.parseInt(num2.trim());
            }
        });

        if(list.size() > 10) {
            for(int i = list.size() - 1; i > 9; i--) {
                list.remove(i);
            }
        }
        if (list.size() > 0) {
            for(int i = 0; i < list.size(); i++) {
                leaderboardList.add((i + 1) + ". " + list.get(i));
            }
        }
        else leaderboardList.add("Sorry, there's no scores.");
    }


}
