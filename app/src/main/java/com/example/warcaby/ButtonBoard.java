package com.example.warcaby;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Random;


public class ButtonBoard extends AppCompatActivity {

    private int[] buttons_id;
    private Button[][] buttonBoard;
    private Button newGame;
    private ArrayList<Cell> moves, highlightedCells;
    private Player player1, player2, currentPlayer;
    private boolean computerMode, computerTurn, srcCellFixed;
    private Board cellBoard = new Board();
    private Cell srcCell, dstCell;
    private Handler delayHandler;
    private Score score = new Score();
    private boolean win = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        cellBoard.initialBoardSetup();
        setContentView(R.layout.board);
        newGame = findViewById(R.id.button7);

        srcCell = null;
        dstCell = null;
        srcCellFixed = false;
        delayHandler = new Handler();
        highlightedCells = new ArrayList<>();
        buttons_id = getButtonArray();
        buttonBoard = new Button[8][8];
        fillButtonBoard(listener);
        updateBoard(buttonBoard, cellBoard);
        this.moves = new ArrayList<>();
        score.resetScores();

        newGame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                finish();
                startActivityForResult(intent, 0);
                overridePendingTransition(0,0);;
            }
        });
        Color();
        Player();


    }


    public void Player() {

        computerMode = true;
        updateTurnTracker();
    }

    public void Color() {


        ButtonBoard.this.player1 = new Player(Piece.LIGHT);
        ButtonBoard.this.player2 = new Player(Piece.DARK);
        ButtonBoard.this.currentPlayer = ButtonBoard.this.player2;
        computerTurn = true;
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                updateTurnTracker();
                computersTurn();
            }
        }, 1000);

        updateTurnTracker();
    }


    private View.OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (Integer) v.getTag();
            int xCord = tag / 10;
            int yCord = tag % 10;

            if (!computerTurn) {
                playerTurn(xCord, yCord);
            }
        }
    };


    public void playerTurn(int xCord, int yCord) {


        if (player1.hasMoves(cellBoard) && player1.hasMoves(cellBoard)) {
            if (cellBoard.getCell(xCord, yCord).containsPiece() && cellBoard.getCell(xCord, yCord).getPiece().getColor().equals(currentPlayer.getColor()) && srcCell == null) {
                unHighlightPieces();    // unhighlight other pieces if user clicks a source cell
                srcCell = cellBoard.getCell(xCord, yCord);
                moves = cellBoard.possibleMoves(srcCell);


                if (moves.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "No possible moves!", Toast.LENGTH_SHORT).show();
                    srcCell = null;
                    updateTurnTracker();
                }

                else {

                    srcCell = cellBoard.getCell(xCord, yCord);
                    updatePiecePressed(srcCell);
                }
            }


            else if (srcCell != null && srcCell.equals(cellBoard.getCell(xCord, yCord)) && !srcCellFixed) {
                srcCell = null;
                updatePieces(xCord, yCord);
                updateTurnTracker();
            } else if (!cellBoard.getCell(xCord, yCord).containsPiece() && moves.contains(cellBoard.getCell(xCord, yCord)) && srcCell != null) {
                dstCell = cellBoard.getCell(xCord, yCord);
                onSecondClick(srcCell, dstCell);
            }

        }


        if ((!player1.hasMoves(cellBoard) && player2.hasMoves(cellBoard)) ||
                (player1.hasMoves(cellBoard) && !player2.hasMoves(cellBoard))) {
            gameOverDialog();
        } else if (!player1.hasMoves(cellBoard) && !player2.hasMoves(cellBoard)) {
            Toast.makeText(getApplicationContext(), "DRAW, NO WINNERS!", Toast.LENGTH_LONG).show();
        }
    }

    public void onSecondClick(Cell givenSrcCell, Cell givenDstCell) {
        unHighlightPieces();
        boolean captureMove = cellBoard.isCaptureMove(givenSrcCell, givenDstCell);
        boolean kingMove = cellBoard.isKingMove(givenSrcCell, givenDstCell);
        boolean kingSrcPiece = givenSrcCell.getPiece().isKing();
        ArrayList<Cell> changedCells = cellBoard.movePiece(givenSrcCell.getCoords(), givenDstCell.getCoords());   // moves piece, store captured piece into array list
        updatePieces(changedCells);

        if(kingMove == true && kingSrcPiece == false) {
            if(computerTurn) {
                score.addScoreTwo(50);
            }
            else {
                score.addScoreOne(50);
            }
        }

        if (captureMove) {
            if(computerTurn) {
                score.addScoreTwo(20);
            }
            else {
                score.addScoreOne(20);
            }

            moves = cellBoard.getCaptureMoves(givenDstCell);

            if (moves.isEmpty()) {
                this.srcCell = null;
                this.dstCell = null;
                srcCellFixed = false;
                changeTurn();

            }
            else {
                this.srcCell = this.dstCell;
                srcCellFixed = true;
                updatePiecePressed(this.srcCell);


                if (currentPlayer == player2 && computerMode) {
                    computerCaptureTurn(moves);
                }
            }
        }

        else {
            srcCell = null;
            dstCell = null;
            srcCellFixed = false;
            changeTurn();
        }
    }


    public void computersTurn() {
        ArrayList<Cell> cellsWithMoves = new ArrayList<>();
        ArrayList<Cell> cellsWithCaptureMoves = new ArrayList<>();

        ArrayList<Cell> captureMoves;

        for (Cell cell : highlightedCells) {
            captureMoves = cellBoard.getCaptureMoves(cell);
            if (!captureMoves.isEmpty()) {
                cellsWithCaptureMoves.add(cell);
            } else {
                cellsWithMoves.add(cell);
            }
        }
        Random random = new Random();

        if (!cellsWithCaptureMoves.isEmpty()) {
            srcCell = cellsWithCaptureMoves.get(random.nextInt(cellsWithCaptureMoves.size()));
            ArrayList<Cell> possibleMoves = cellBoard.getCaptureMoves(srcCell);
            dstCell = possibleMoves.get(random.nextInt(possibleMoves.size()));
        } else {
            srcCell = cellsWithMoves.get(random.nextInt(cellsWithMoves.size()));
            ArrayList<Cell> possibleMoves = cellBoard.possibleMoves(srcCell);
            dstCell = possibleMoves.get(random.nextInt(possibleMoves.size()));
        }

        updatePiecePressed(srcCell);


        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onSecondClick(srcCell, dstCell);
            }
        }, 1000);
    }


    public void computerCaptureTurn(ArrayList<Cell> captureMoves) {
        dstCell = captureMoves.get(new Random().nextInt(captureMoves.size()));
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onSecondClick(srcCell, dstCell);
            }
        }, 1000);
    }



    public int[] getButtonArray() {
        int[] buttons_id = {R.id.button0, R.id.button2, R.id.button4, R.id.button6,
                R.id.button9, R.id.button11, R.id.button13, R.id.button15,
                R.id.button16, R.id.button18, R.id.button20, R.id.button22,
                R.id.button25, R.id.button27, R.id.button29, R.id.button31,
                R.id.button32, R.id.button34, R.id.button36, R.id.button38,
                R.id.button41, R.id.button43, R.id.button45, R.id.button47,
                R.id.button48, R.id.button50, R.id.button52, R.id.button54,
                R.id.button57, R.id.button59, R.id.button61, R.id.button63};
        return buttons_id;
    }


    public void fillButtonBoard(View.OnClickListener listener) {
        int index = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if ((i + j) % 2 == 0) {
                    buttonBoard[i][j] = (Button) findViewById(buttons_id[index]);
                    index++;
                    buttonBoard[i][j].setTag(i * 10 + j);
                    buttonBoard[i][j].setOnClickListener(listener);
                }
            }
        }
    }


    public void updateBoard(Button[][] buttonIndexes, Board board) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if ((i + j) % 2 == 0) {
                    if (!board.getCell(i, j).containsPiece()) {
                        buttonIndexes[i][j].setBackgroundResource(R.drawable.blank_square);
                    }

                    else if (board.getCell(i, j).getPiece().getColor().equals(Piece.LIGHT)) {

                        if (board.getCell(i, j).getPiece().isKing()) {
                            buttonIndexes[i][j].setBackgroundResource(R.drawable.light_king_piece);
                        }

                        else {
                            buttonIndexes[i][j].setBackgroundResource(R.drawable.light_piece);
                        }
                    }
                    else if (board.getCell(i, j).getPiece().getColor().equals(Piece.DARK)) {
                        if (board.getCell(i, j).getPiece().isKing()) {
                            buttonIndexes[i][j].setBackgroundResource(R.drawable.dark_king_piece);
                        }

                        else {
                            buttonIndexes[i][j].setBackgroundResource(R.drawable.dark_piece);
                        }
                    }
                }
            }
        }
    }


    public void updatePieces(int xCord, int yCord) {

        Cell possMoves;
        for (int i = 0; i < moves.size(); i++) {
            possMoves = moves.get(i);
            buttonBoard[possMoves.getX()][possMoves.getY()].setBackgroundResource(R.drawable.blank_square);   // color possible moves blank
        }


        if (cellBoard.getCell(xCord, yCord).getPiece().getColor().equals(Piece.LIGHT) && cellBoard.getCell(xCord, yCord).containsPiece()) {
            if (cellBoard.getCell(xCord, yCord).getPiece().isKing()) {
                buttonBoard[xCord][yCord].setBackgroundResource(R.drawable.light_king_piece);
            }

            else {
                buttonBoard[xCord][yCord].setBackgroundResource(R.drawable.light_piece);
            }
        }
        else {
            if (cellBoard.getCell(xCord, yCord).getPiece().isKing()) {
                buttonBoard[xCord][yCord].setBackgroundResource(R.drawable.dark_king_piece);
            }

            else {
                buttonBoard[xCord][yCord].setBackgroundResource(R.drawable.dark_piece);
            }
        }
    }


    public void updatePieces(ArrayList<Cell> changedCells) {


        Cell possMoves;
        for (int i = 0; i < moves.size(); i++) {
            possMoves = moves.get(i);
            buttonBoard[possMoves.getX()][possMoves.getY()].setBackgroundResource(R.drawable.blank_square);   // color possible moves blank
        }

        for (Cell cell : changedCells) {
            if (!cell.containsPiece()) {
                buttonBoard[cell.getX()][cell.getY()].setBackgroundResource(R.drawable.blank_square);
            } else if (cell.getPiece().getColor().equals(Piece.LIGHT)) {
                if (cell.getPiece().isKing()) {
                    buttonBoard[cell.getX()][cell.getY()].setBackgroundResource(R.drawable.light_king_piece);
                } else {
                    buttonBoard[cell.getX()][cell.getY()].setBackgroundResource(R.drawable.light_piece);
                }
            } else if (cell.getPiece().getColor().equals(Piece.DARK)) {
                if (cell.getPiece().isKing()) {
                    buttonBoard[cell.getX()][cell.getY()].setBackgroundResource(R.drawable.dark_king_piece);
                } else {
                    buttonBoard[cell.getX()][cell.getY()].setBackgroundResource(R.drawable.dark_piece);
                }
            }
        }
    }


    public void updatePiecePressed(Cell givenCell) {

        if (currentPlayer.getColor().equals(Piece.LIGHT) && givenCell.getPiece().getColor().equals(Piece.LIGHT)) {


            if (givenCell.getPiece().isKing()) {
                buttonBoard[givenCell.getX()][givenCell.getY()].setBackgroundResource(R.drawable.light_king_piece_pressed);
            }

            else {
                buttonBoard[givenCell.getX()][givenCell.getY()].setBackgroundResource(R.drawable.light_piece_pressed);  // fill selected light piece as pressed piece image
            }
        }

        if (currentPlayer.getColor().equals(Piece.DARK) && givenCell.getPiece().getColor().equals(Piece.DARK)) {


            if (cellBoard.getCell(givenCell.getX(), givenCell.getY()).getPiece().isKing()) {
                buttonBoard[givenCell.getX()][givenCell.getY()].setBackgroundResource(R.drawable.dark_king_piece_pressed);
            }

            else {
                buttonBoard[givenCell.getX()][givenCell.getY()].setBackgroundResource(R.drawable.dark_piece_pressed);   // fill selected dark piece as pressed piece image
            }
        }
    }


    public void changeTurn() {

        if (player1.hasMoves(cellBoard) && player2.hasMoves(cellBoard)) {
            if (this.currentPlayer.equals(player1)) {
                this.currentPlayer = player2;


                computerTurn = true;
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        computersTurn();
                    }
                }, 1000);

                updateTurnTracker();

            } else {
                this.currentPlayer = player1;


                computerTurn = false;

                updateTurnTracker();
            }
        } else {
            gameOverDialog();
        }
    }

    public void unHighlightPieces() {
        Cell highlightedCell;
        while (!highlightedCells.isEmpty()) {
            highlightedCell = highlightedCells.remove(0);
            if (highlightedCell.getPiece().getColor().equals(Piece.LIGHT)) {
                if (highlightedCell.getPiece().isKing()) {
                    buttonBoard[highlightedCell.getX()][highlightedCell.getY()].setBackgroundResource(R.drawable.light_king_piece);
                } else {
                    buttonBoard[highlightedCell.getX()][highlightedCell.getY()].setBackgroundResource(R.drawable.light_piece);
                }
            } else {
                if (highlightedCell.getPiece().isKing()) {
                    buttonBoard[highlightedCell.getX()][highlightedCell.getY()].setBackgroundResource(R.drawable.dark_king_piece);
                } else {
                    buttonBoard[highlightedCell.getX()][highlightedCell.getY()].setBackgroundResource(R.drawable.dark_piece);
                }
            }
        }
    }

    public void updateTurnTracker() {
        if (this.currentPlayer != null) {

            ArrayList<Piece> currentPlayerPieces = cellBoard.getPieces(this.currentPlayer.getColor());
            ArrayList<Cell> moves;

            for (Piece piece : currentPlayerPieces) {
                moves = cellBoard.possibleMoves(piece);
                if (!moves.isEmpty()) {
                    if (piece.getColor().equals(Piece.DARK) && piece.isKing()) {
                        buttonBoard[piece.getCell().getX()][piece.getCell().getY()].setBackgroundResource(R.drawable.dark_king_highlighted);
                    } else if (piece.getColor().equals(Piece.DARK)) {
                        buttonBoard[piece.getCell().getX()][piece.getCell().getY()].setBackgroundResource(R.drawable.dark_piece_highlighted);
                    } else if (piece.getColor().equals(Piece.LIGHT) && piece.isKing()) {
                        buttonBoard[piece.getCell().getX()][piece.getCell().getY()].setBackgroundResource(R.drawable.light_king_highlighted);
                    } else if (piece.getColor().equals(Piece.LIGHT)) {
                        buttonBoard[piece.getCell().getX()][piece.getCell().getY()].setBackgroundResource(R.drawable.light_piece_highlighted);
                    }
                    highlightedCells.add(piece.getCell());
                }
            }
        }
    }


    public void gameOverDialog() {
        if(win == false) {
            win = true;
            updateTurnTracker();
            String winner;
            if (!player1.hasMoves(cellBoard)) {
                winner = "Player 2";
                score.addScoreTwo(300);
                score.addScoreOne(100);
            } else {
                winner = "Player 1";
                score.addScoreOne(300);
                score.addScoreTwo(100);
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(ButtonBoard.this);
            builder.setCancelable(false);
            builder.setTitle(winner + " Wins!");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    score.setName(ButtonBoard.this);
                    dialog.cancel();
                }
            });
            builder.show();
        }



    }
}