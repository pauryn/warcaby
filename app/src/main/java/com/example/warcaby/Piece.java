
package com.example.warcaby;

public class Piece {

	public static final String DARK = "Dark";
	public static final String LIGHT = "Light";
	private String color;
	private boolean isKing;
	private Cell placedCell;


	public Piece(String color){
		this.color = color;
		this.isKing = false;
		this.placedCell = null;
	}

	public static String getOpponentColor(String givenColor){
		if(givenColor.equals(Piece.DARK)){
			return Piece.LIGHT;
		}
		else if(givenColor.equals(Piece.LIGHT)){
			return Piece.DARK;
		}
		else{
			System.out.println("Given color is not valid. Given color: " + givenColor);
			return null;
		}
	}

	public Cell getCell(){
		return this.placedCell;
	}

	public void setCell(Cell givenCell){
		this.placedCell = givenCell;
	}

	public String getColor(){
		return this.color;
	}

	public boolean isKing(){
		return this.isKing;
	}

	public void makeKing(){
		this.isKing = true;
	}

	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof Piece)){
			return false;
		}

		Piece givenPiece =  (Piece) obj;

		if(givenPiece.getColor().equals(this.color) && givenPiece.isKing() == this.isKing &&
				givenPiece.getCell().getX() == this.placedCell.getX() && givenPiece.getCell().getY() == this.placedCell.getY()){
			return true;
		}
		return false;
	}
}