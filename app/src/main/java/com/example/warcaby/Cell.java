package com.example.warcaby;

public class Cell {


	private int x, y;
	private Piece placedPiece;


	public Cell(int x, int y){
		if((x<0 || x>7) || (y<0 || y>7)){
			System.out.println("The provided coordinates for the cell are out of range.");
			return;
		}
		this.x = x;
		this.y = y;
		this.placedPiece = null;
	}



	public Piece getPiece(){
		return this.placedPiece;
	}


	public int getX(){
		return this.x;
	}


	public int getY(){
		return this.y;
	}


	public int[] getCoords(){
		int[] coords = {x, y};
		return coords;
	}


	public void placePiece(Piece givenPiece){
		this.placedPiece = givenPiece;
		if(givenPiece != null){
			givenPiece.setCell(this);
			if(this.x == 0 && givenPiece.getColor().equals(Piece.DARK)){
				this.placedPiece.makeKing();
			}
			else if(this.x == 7 && givenPiece.getColor().equals(Piece.LIGHT)){
				this.placedPiece.makeKing();
			}
		}
	}


	public boolean containsPiece(){
		return (this.placedPiece != null);
	}



	public void movePiece(Cell anotherCell) throws IllegalArgumentException{
		if(anotherCell == null){
			throw new IllegalArgumentException("Provided cell is null. Cannot move to a null Cell.");
		}
		anotherCell.placePiece(this.placedPiece);
		this.placedPiece.setCell(anotherCell);
		this.placedPiece = null;
	}



	public String toString(){
		String str = "";
		str += "Cell Loc: ("+ this.x + ", " + this.y + ") \t Placed piece: ";
		if(this.placedPiece == null){
			str += "nothing\n";
		}
		else{
			str += this.placedPiece.getColor() + "  isKing: " + placedPiece.isKing()+ "\n";
		}
		return str;
	}
}



