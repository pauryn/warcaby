package com.example.warcaby;

import java.util.ArrayList;

public class Board {

    private static int BOARD_SIZE = 8;
    private Cell[][] board;
    private ArrayList<Piece> lightPieces, darkPieces;


    public Board(){
        this.lightPieces = new ArrayList<Piece>();
        this.darkPieces = new ArrayList<Piece>();
        board = new Cell[Board.BOARD_SIZE][Board.BOARD_SIZE];
    }


    public void initialBoardSetup(){
        for(int i=0; i < Board.BOARD_SIZE; i++) {
            for (int j = 0; j < Board.BOARD_SIZE; j++) {
                this.board[i][j] = new Cell(i, j);
            }
        }

        for(int column = 0; column < Board.BOARD_SIZE; column+= 2){
            this.board[0][column].placePiece(new Piece(Piece.LIGHT));
            this.board[2][column].placePiece(new Piece(Piece.LIGHT));
            this.board[6][column].placePiece(new Piece(Piece.DARK));

            lightPieces.add(this.board[0][column].getPiece());
            lightPieces.add(this.board[2][column].getPiece());
            darkPieces.add(this.board[6][column].getPiece());
        }

        for(int column = 1; column< Board.BOARD_SIZE; column+=2){
            this.board[1][column].placePiece(new Piece(Piece.LIGHT));
            this.board[5][column].placePiece(new Piece(Piece.DARK));
            this.board[7][column].placePiece(new Piece(Piece.DARK));

            lightPieces.add(this.board[1][column].getPiece());
            darkPieces.add(this.board[5][column].getPiece());
            darkPieces.add(this.board[7][column].getPiece());
        }
    }


    public Cell getCell(int x, int y) throws IllegalArgumentException{
        if((x<0 || x > 7) || (y<0 || y >7)){
            throw new IllegalArgumentException("The coordinates provided are outside of the board");
        }

        return this.board[x][y];
    }


    public ArrayList<Piece> getPieces(String givenColor) throws IllegalArgumentException{
        if(givenColor.equals(Piece.LIGHT)){
            return this.lightPieces;
        }
        else if(givenColor.equals(Piece.DARK)){
            return this.darkPieces;
        }
        throw new IllegalArgumentException("Given color is not the color of the pieces in board. Given color: " + givenColor);
    }


    public ArrayList<Cell> movePiece(int fromX, int fromY, int toX, int toY) throws NullPointerException, IllegalArgumentException{
        Cell srcCell = this.getCell(fromX, fromY);
        Cell dstCell = this.getCell(toX, toY);
        ArrayList<Cell> changedCells = new ArrayList<Cell>();
        if(srcCell.getPiece() == null){
            throw new NullPointerException("The source cell does not contains piece to move.");
        }
        if(dstCell.getPiece() != null){
            throw new IllegalArgumentException("The destination cell already contains a piece. Cannot move to occupied cell.");
        }


        if(isCaptureMove(srcCell, dstCell)){
            int capturedCellX = (fromX + toX)/ 2;
            int capturedCellY= (fromY + toY)/2;
            Piece capturedPiece = this.board[capturedCellX][capturedCellY].getPiece();
            removePiece(capturedPiece);
            changedCells.add(capturedPiece.getCell());
        }
        srcCell.movePiece(dstCell);
        changedCells.add(srcCell);
        changedCells.add(dstCell);
        return changedCells;
    }


    public ArrayList<Cell> movePiece(int[] src, int[] dst) throws IllegalArgumentException{
        if(src.length != 2 || dst.length != 2){
            throw new IllegalArgumentException("The given dimension of the points does not match.");
        }
        return movePiece(src[0], src[1], dst[0], dst[1]);
    }


    public ArrayList<Cell> movePiece(int[] move) throws IllegalArgumentException{
        if(move.length != 4){
            throw new IllegalArgumentException("The given dimension of the points does not match.");
        }
        return movePiece(move[0], move[1], move[2], move[3]);
    }


    public void removePiece(Piece capturedPiece) throws IllegalStateException, IllegalArgumentException{
        if(capturedPiece.getColor().equals(Piece.LIGHT)){
            if(!lightPieces.remove(capturedPiece)){
                throw new IllegalStateException("Error removing the piece");
            }
            capturedPiece.getCell().placePiece(null);
        }
        else if(capturedPiece.getColor().equals(Piece.DARK)){
            if(!darkPieces.remove(capturedPiece)){
                throw new IllegalStateException("Error removing the piece");
            }
            capturedPiece.getCell().placePiece(null);
        }
    }


    public ArrayList<Cell> possibleMoves(int x, int y) throws IllegalArgumentException{
        if((x<0 || x>7) || (y<0 || y>7)){
            throw new IllegalArgumentException("Invalid value of x or y provided. (x, y) = (" + x +", " + ")");
        }
        return possibleMoves(this.board[x][y]);
    }


    public ArrayList<Cell> possibleMoves(Cell givenCell) throws NullPointerException{

        if(givenCell == null){
            throw new NullPointerException("Given Cell is null. Cannot find the possible moves of null Cell");
        }

        ArrayList<Cell> nextMoves = new ArrayList<Cell>();
        Piece givenPiece = givenCell.getPiece();

        if(givenPiece == null){
            return nextMoves;
        }

        String playerColor = givenPiece.getColor();
        String opponentColor = Piece.getOpponentColor(playerColor);



        if(playerColor.equals(Piece.LIGHT)){

            int nextX = givenCell.getX()+1;

            if(nextX < 8){

                int nextY = givenCell.getY()+1;

                if(nextY < 8){

                    if(!this.board[nextX][nextY].containsPiece()){
                        nextMoves.add(this.board[nextX][nextY]);
                    }

                    else if(this.board[nextX][nextY].getPiece().getColor().equals(opponentColor)){
                        int xCoordAfterHoping = nextX + 1;
                        int yCoordAfterHoping = nextY + 1;
                        if(xCoordAfterHoping < 8 && yCoordAfterHoping < 8 && !this.board[xCoordAfterHoping][yCoordAfterHoping].containsPiece()){
                            nextMoves.add(this.board[xCoordAfterHoping][yCoordAfterHoping]);
                        }
                    }
                }



                nextY = givenCell.getY() -1;

                if(nextY >=0){
                    if(!this.board[nextX][nextY].containsPiece()){
                        nextMoves.add(this.board[nextX][nextY]);
                    }

                    else if(this.board[nextX][nextY].getPiece().getColor().equals(opponentColor)){
                        int xCoordAfterHoping = nextX + 1;
                        int yCoordAfterHoping = nextY - 1;
                        if(xCoordAfterHoping < 8 && yCoordAfterHoping >= 0 && !this.board[xCoordAfterHoping][yCoordAfterHoping].containsPiece()){
                            nextMoves.add(this.board[xCoordAfterHoping][yCoordAfterHoping]);
                        }
                    }
                }
            }


            if(givenPiece.isKing()){
                nextX = givenCell.getX() -1;
                if(nextX >=0){

                    int nextY = givenCell.getY()+1;
                    if(nextY < 8 && !this.board[nextX][nextY].containsPiece()){
                        nextMoves.add(this.board[nextX][nextY]);
                    }

                    else if(nextY < 8 && this.board[nextX][nextY].getPiece().getColor().equals(opponentColor)){
                        int xCoordAfterHoping = nextX - 1;
                        int yCoordAfterHoping = nextY + 1;
                        if(xCoordAfterHoping >=0 && yCoordAfterHoping < 8 && !this.board[xCoordAfterHoping][yCoordAfterHoping].containsPiece()){
                            nextMoves.add(this.board[xCoordAfterHoping][yCoordAfterHoping]);
                        }
                    }

                    nextY = givenCell.getY() -1;
                    if(nextY >=0 && !this.board[nextX][nextY].containsPiece()){
                        nextMoves.add(this.board[nextX][nextY]);
                    }

                    else if(nextY >=0 && this.board[nextX][nextY].getPiece().getColor().equals(opponentColor)){
                        int xCoordAfterHoping = nextX - 1;
                        int yCoordAfterHoping = nextY - 1;
                        if(xCoordAfterHoping >=0 && yCoordAfterHoping >= 0 && !this.board[xCoordAfterHoping][yCoordAfterHoping].containsPiece()){
                            nextMoves.add(this.board[xCoordAfterHoping][yCoordAfterHoping]);
                        }
                    }
                }
            }
        }


        else if(givenPiece.getColor().equals(Piece.DARK)){


            int nextX = givenCell.getX()-1;
            if(nextX >= 0){

                int nextY = givenCell.getY()+1;
                if(nextY < 8 && !this.board[nextX][nextY].containsPiece()){
                    nextMoves.add(this.board[nextX][nextY]);
                }

                else if(nextY < 8 && this.board[nextX][nextY].getPiece().getColor().equals(opponentColor)){
                    int xCoordAfterHoping = nextX -1;
                    int yCoordAfterHoping = nextY +1;
                    if(xCoordAfterHoping >=0 && yCoordAfterHoping < 8 && !this.board[xCoordAfterHoping][yCoordAfterHoping].containsPiece()){
                        nextMoves.add(this.board[xCoordAfterHoping][yCoordAfterHoping]);
                    }
                }

                nextY = givenCell.getY()-1;
                if(nextY >=0 && !this.board[nextX][nextY].containsPiece()){
                    nextMoves.add(this.board[nextX][nextY]);
                }
                else if(nextY >=0 && this.board[nextX][nextY].getPiece().getColor().equals(opponentColor)){
                    int xCoordAfterHoping = nextX -1;
                    int yCoordAfterHoping = nextY - 1;
                    if(xCoordAfterHoping >=0 && yCoordAfterHoping >=0 && !this.board[xCoordAfterHoping][yCoordAfterHoping].containsPiece()){
                        nextMoves.add(this.board[xCoordAfterHoping][yCoordAfterHoping]);
                    }
                }
            }


            if(givenPiece.isKing()){

                nextX = givenCell.getX()+1;
                if(nextX < 8){

                    int nextY = givenCell.getY()+1;
                    if(nextY < 8 && !this.board[nextX][nextY].containsPiece()){
                        nextMoves.add(this.board[nextX][nextY]);
                    }

                    else if(nextY < 8 && this.board[nextX][nextY].getPiece().getColor().equals(opponentColor)){
                        int xCoordAfterHoping = nextX + 1;
                        int yCoordAfterHoping = nextY +  1;
                        if(xCoordAfterHoping < 8 && yCoordAfterHoping < 8 && !this.board[xCoordAfterHoping][yCoordAfterHoping].containsPiece()){
                            nextMoves.add(this.board[xCoordAfterHoping][yCoordAfterHoping]);
                        }
                    }

                    nextY = givenCell.getY() -1;
                    if(nextY >=0 && !this.board[nextX][nextY].containsPiece()){
                        nextMoves.add(this.board[nextX][nextY]);
                    }
                    else if(nextY >=0 && this.board[nextX][nextY].getPiece().getColor().equals(opponentColor)){
                        int xCoordAfterHoping = nextX + 1;
                        int yCoordAfterHoping = nextY -  1;
                        if(xCoordAfterHoping < 8 && yCoordAfterHoping >= 0 && !this.board[xCoordAfterHoping][yCoordAfterHoping].containsPiece()){
                            nextMoves.add(this.board[xCoordAfterHoping][yCoordAfterHoping]);
                        }
                    }
                }
            }
        }

        return nextMoves;
    }


    public ArrayList<Cell> possibleMoves(Piece givenPiece) throws  NullPointerException{
        if(givenPiece == null){
            throw new NullPointerException("The Piece provided is null. Cannot find possible moves of a null Piece");
        }
        return possibleMoves(givenPiece.getCell());
    }

    public ArrayList<Cell> getCaptureMoves(Cell givenCell) throws NullPointerException{
        if(givenCell == null){
            throw new NullPointerException("The Cell provided is null.");
        }
        ArrayList<Cell> possibleMovesOfCell = possibleMoves(givenCell);

        ArrayList<Cell> capturingMoves = new ArrayList<Cell>();

        for(Cell dstCell: possibleMovesOfCell){
            if(isCaptureMove(givenCell, dstCell)){
                capturingMoves.add(dstCell);
            }
        }
        return capturingMoves;
    }

    public ArrayList<Cell> getCaptureMoves(int x, int y) throws  IllegalArgumentException{
        if((x<0 || x>7) || (y<0 || y>7)){
            throw new IllegalArgumentException("Invalid value of x or y provided. (x, y) = (" + x +", " + ")");
        }
        return getCaptureMoves(this.board[x][y]);
    }

    public boolean isCaptureMove(Cell srcCell, Cell dstCell) throws NullPointerException, IllegalArgumentException {

        if ((Math.abs(srcCell.getX() - dstCell.getX()) == 2) && (Math.abs(srcCell.getY() - dstCell.getY()) == 2)) {
            return true;
        }
        return false;
    }

    public boolean isKingMove (Cell srcCell, Cell dstCell) {

        if ((srcCell.getX() == 6 && dstCell.getX() == 7) || (srcCell.getX() == 5 && dstCell.getX() == 7)  || (srcCell.getX() == 1 && dstCell.getY() == 0) || (srcCell.getX() == 1 && dstCell.getY() == 0)) {
            return true;
        }
        return false;
    }


}
